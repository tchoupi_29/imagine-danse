<header>
    <nav id="barre" class="navbar navbar-light">
        <div class="container-fluid menu">
            <button id="btnMenu" class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Afficher le menu">
                <span class="line"></span> 
                <span class="line"></span> 
                <span class="line" style="margin-bottom: 0;"></span>
            </button>
            <div class="col-1">
                <a href="index.php"><img id="imgLogo" class="logo" src="res/images/icones/logoEntier.png" alt="logo imagine"></a>
            </div>
        </div>
    </nav>
    <div class="collapse row" id="navbarToggleExternalContent">
        <div id="menu" class="p-4 lienNav">
            <div>
                <a href="index.php">Accueil</a>
            </div>
            <div>
                <a href="decouvrir.php">Découvrir Imagine Danse...</a>
            </div>
            <div>
                <a href="informations.php">Informations Pratiques</a>
            </div>
            <div>
                <a href="inscription.php">Inscription</a>
            </div>
            <div>
                <a href="contact.php">Contact</a>
            </div>
        </div>
    </div>
    <script>
        var button = document.getElementById("btnMenu");
        var logo = document.getElementById("imgLogo");
        var menu = document.getElementById("menu");
        var nav = document.getElementById("barre");

        var animationEnCours = false;
        
        button.addEventListener("click", function() {
            if (animationEnCours) {
                return;
            }

            animationEnCours = true;

            if (menu.classList.contains("bouger")) {
                logo.classList.remove("grandir");
                menu.classList.remove("bouger");
            } else {
                logo.classList.add("grandir");
                menu.classList.add("bouger");
                nav.style.borderBottomStyle = "none";
            }

            logo.addEventListener('transitionend', function() {
                animationEnCours = false;
                if (!menu.classList.contains("bouger")){
                    nav.style.borderBottomStyle = "solid";
                } else {
                    nav.style.borderBottomStyle = "none";
                }
            });
        });
    </script>
</header>