<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Post créé</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="creerSuccess.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md">
                            <p>
                                Le Carrousel a été modifié avec succès !
                            </p>
                        </div>
                    </div>
                    <div class="row boutons">
                        <div class="col-md-2 boutonCreer">                           
                            <a class="btn btn-primary" href="modifierCarrousel.php">Continuer les modifications</a>
                        </div>
                        <div class="col-md-2 boutonRetour">
                            <a class="btn btn-primary" href="administration.php">Retour à l'accueil</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>