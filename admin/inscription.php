<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Administration des Inscriptions</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="inscription.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <?php require("../scripts/inscription.php"); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md">
                            <h3>Administration des Inscriptions</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <h4>Statut des Inscriptions</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-3 paragaphe">
                                <?php
                                    $status = insriptionStatus();
                                    if ($status == true) {
                                        echo '<p>Les Inscriptions sont ouvertes</p>';
                                        $mot = "Fermer";
                                    } else {
                                        echo '<p>Les Inscriptions sont fermées</p>';
                                        $mot = "Ouvrir";
                                    }
                                ?>
                            </div>
                            <div class="col-md-2 boutonOuvrir paragaphe">
                                <a class="btn btn-primary" href="scripts/changeInscriptionsStatus.php"><?php echo $mot ?> les Inscriptions</a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <h4>Bulletin d'inscription</h4>
                        </div>
                        <?php
                        if (isset($_GET["erreur"])){
                            if ($_GET["erreur"] == 1){
                                echo '<div class="row">';
                                    echo '<div class="col-md">';
                                        echo '<p class="erreur">Le fichier sélectioné n\'est pas un pdf !</p>';
                                    echo '</div>';
                                echo '</div>';
                            } else if ($_GET["erreur"] == 2){
                                echo '<div class="row">';
                                    echo '<div class="col-md">';
                                        echo '<p class="erreur">Le fichier sélectioné est trop volumineux !</p>';
                                    echo '</div>';
                                echo '</div>';
                            }
                        } else if (isset($_GET["success"])){
                            echo '<div class="row">';
                                    echo '<div class="col-md">';
                                        echo '<p class="success">Le fichier sélectioné à bien été enregistré !</p>';
                                    echo '</div>';
                                echo '</div>';
                        }
                        ?>
                        <div class="row">
                            <div class="col-md-3 paragaphe">
                                <p>Modifier le bulletin d'inscription, séléctioner le nouveau bulletin d'inscription (PDF)</p>
                            </div>
                            <div class="col-md paragaphe">
                                <form class="form" action="scripts/changeBultin.php" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <input type="file" id="bultin" name="bultin" accept="application/pdf" required>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="btn btn-primary" id="boutonValider" type="submit" name="submit" value="Valider">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>