<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Administration du Blog</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="blog.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md">
                            <h3>Administration du Blog</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <h4>Liste des Posts</h4>
                        </div>
                        <div class="col-md boutonCreer">
                            <a class="btn btn-primary" href="creerPost.php">Créer un Post</a>
                        </div>
                    </div>
                    <!-- affichages des post -->
                    <?php
                        require('scripts/post.php');
                        if (!isset($_GET["page"])){
                            $page = 0;
                        } else {
                            $page = $_GET["page"];
                        }
                        $posts = getPost($page, 20);
                        foreach ($posts as $post) {
                            echo '<div class="row art justify-content-center">';
                                echo '<a href="detailPost.php?idPost=' . $post["id"] . '" class="row titreArt">';
                                    echo '<div class="col-md">';
                                        echo '<h3>'. $post["titre"] .'</h3>';
                                    echo '</div>';
                                    echo '<div class="col-md heurePubli">';
                                        echo '<p>' . $post["date_de_creation"] . ' '. $post["heure_de_creation"] . '</p>';
                                    echo '</div>';
                                echo '</a>';
                                echo '<div class="row boutons">';
                                    echo '<div class="col-md-1">';
                                        echo '<div class="col-md boutonSupprimer">';
                                            echo '<a class="btn btn-primary" href="supprimerPost.php?idPost=' . $post["id"] . '">Supprimer</a>';
                                        echo '</div>';
                                    echo '</div>';
                                    echo '<div class="col-md-1">';
                                        echo '<div class="col-md boutonModifier">';
                                            echo '<a class="btn btn-primary" href="modifierPost.php?idPost=' . $post["id"] . '">Modifier</a>';
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        }
                    ?>
                    <div class="row">
                        <div class="col-md text-center controlPage">
                            <?php
                                $maxPage = getMaxPage();
                                if ($page - 1 >= 0){
                                    echo '<a href="blog.php?page=' . $page - 1 . '">←</a>';
                                }
                                if ($maxPage > 0) {
                                    echo $page+1 . '/' . $maxPage+1;
                                }
                                if ($page + 1 <= $maxPage){
                                    echo '<a href="blog.php?page=' . $page + 1 . '">→</a>';
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>
