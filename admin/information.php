<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Administration des Informations Pratiques</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="information.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <?php require("../scripts/inscription.php"); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md">
                            <h3>Administration des Informations Pratiques</h3>
                        </div>
                    </div>
                    <div class="row">
                        <?php
                        if (isset($_GET["erreur"])){
                            if ($_GET["erreur"] == 1){
                                echo '<div class="row">';
                                    echo '<div class="col-md">';
                                        echo '<p class="erreur">Le fichier sélectioné n\'est pas une image !</p>';
                                    echo '</div>';
                                echo '</div>';
                            } else if ($_GET["erreur"] == 2){
                                echo '<div class="row">';
                                    echo '<div class="col-md">';
                                        echo '<p class="erreur">Le fichier sélectioné est trop volumineux !</p>';
                                    echo '</div>';
                                echo '</div>';
                            }
                        } else if (isset($_GET["success"])){
                            echo '<div class="row">';
                                    echo '<div class="col-md">';
                                        echo '<p class="success">Le fichier sélectioné à bien été enregistré !</p>';
                                    echo '</div>';
                                echo '</div>';
                        }
                        ?>
                        <div class="row">
                            <div class="col-md">
                                <h4>Tableau des Horaires</h4>
                            </div>
                        </div>
                        <div class="row champ">
                            <div class="col-md-3 paragaphe">
                                <p>Modifier le tableau des Horaires, séléctioner le nouveau tableau des Horaires (png, jpg)</p>
                            </div>
                            <div class="col-md paragaphe">
                                <form class="form" action="scripts/changeInformation.php" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <input type="file" id="horaires" name="horaires" accept="image/*" required>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="btn btn-primary" id="boutonValider" type="submit" name="submit" value="Valider">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <h4>Tableau des Tarifs</h4>
                            </div>
                        </div>
                        <div class="row champ">
                            <div class="col-md-3 paragaphe">
                                <p>Modifier le tableau des Tarifs, séléctioner le nouveau tableau des Tarifs (png, jpg)</p>
                            </div>
                            <div class="col-md paragaphe">
                                <form class="form" action="scripts/changeInformation.php" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <input type="file" id="tarifs" name="tarifs" accept="image/*" required>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="btn btn-primary" id="boutonValider" type="submit" name="submit" value="Valider">
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <h4>Tableau du Planning de l'année</h4>
                            </div>
                        </div>
                        <div class="row champ">
                            <div class="col-md-3 paragaphe">
                                <p>Modifier le tableau du Planning, séléctioner le nouveau tableau du Planning (png, jpg)</p>
                            </div>
                            <div class="col-md paragaphe">
                                <form class="form" action="scripts/changeInformation.php" method="post" enctype="multipart/form-data">
                                    <div class="row">
                                        <input type="file" id="planning" name="planning" accept="image/*" required>
                                    </div>
                                    <div class="col-md-2">
                                        <input class="btn btn-primary" id="boutonValider" type="submit" name="submit" value="Valider">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>