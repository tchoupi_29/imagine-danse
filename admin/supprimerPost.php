<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Confirmer la suppression</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="supprimerPost.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md">
                            <p>
                                Êtes vous sûr de vouloir supprimer ce post ?
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-1 boutonValider">
                            <?php echo '<a class="btn btn-primary" href="scripts/delPost.php?idPost=' . $_GET["idPost"] . '">Supprimer</a>' ?>
                        </div>
                        <div class="col-md-1 boutonAnnuler">
                            <a class="btn btn-primary" href="blog.php">Annuler</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>