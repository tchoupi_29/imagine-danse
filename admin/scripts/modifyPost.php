<?php
    session_start();
    include('../../connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }

    $uploadOk = true;

    $idPost = intval($_POST["idPost"]);

    $fichierPresent = false;
    if (isset($_FILES["fichiers"]["name"])) {
        foreach ($_FILES["fichiers"]["name"] as $name) {
            if (!empty($name)) {
                $fichierPresent = true;
            }
        }   
    }

    $maxFic = 0;
    $fics = array_diff(scandir("../../res/images/blog/".$idPost."/"), array(".", ".."));
    if (!empty($fics)) {
        foreach ($fics as $fic) {
            $nb = intval(substr($fic, 0, 1));
            if ($nb > $maxFic) {
                $maxFic = $nb;
            }
        }
    }

    if (isset($_POST["ficASuppr"])) {
        foreach ($_POST["ficASuppr"] as $fic) {
            unlink('../../res/images/blog/' . $idPost . '/' . $fic);
        }
    }
    

    // on set une variable de session pour retenir les champ si on retourne sur la page de creation
    $_SESSION["modificationPost"] = array("contenu" => $_POST["contenu"], "titre" => $_POST["titre"]);

    if ($fichierPresent == true){
        $compteur = 0;
        // on ajout ces meme fic dans dossier
        for ($i=0; $i < sizeof($_FILES["fichiers"]["tmp_name"])-1 ; $i++) {
            // max file size : 8 Mo
            if ($_FILES["fichiers"]["size"][$i] > 8000000) {
                $uploadOk = false;
                header('Location: ../creerPost.php?erreur=1');
            }

            if ($compteur >= 20) {
                $uploadOk = false;
                header('Location: ../creerPost.php?erreur=2');
            }

            if ($uploadOk == true && !empty($_FILES["fichiers"]["tmp_name"][$i])) {
                $compteur = $compteur + 1;
                $nom = ($compteur + $maxFic) . ' ' .explode(".", $_FILES["fichiers"]["name"][$i])[0];
                move_uploaded_file($_FILES["fichiers"]["tmp_name"][$i], "../../res/images/blog/" . ($idPost) . "/" . $nom.'.'.explode(".", $_FILES["fichiers"]["name"][$i])[1]);
            }
        }
    }

    if ($uploadOk == true) {
        $sth = $dbh->prepare('UPDATE post set titre = ?, contenu = ? where id = ?');
        $sth -> execute(array(htmlspecialchars($_POST["titre"]), htmlspecialchars($_POST["contenu"]), $idPost));
        unset($_SESSION["modificationPost"]);
        header('Location: ../modifierSuccess.php?idPost='.$idPost);
    }
?>
