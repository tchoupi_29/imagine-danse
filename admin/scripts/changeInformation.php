<?php
    session_start();
    $uploadOk = true;

    if (isset($_FILES["horaires"])){
        $file = "horaires";
    } else if (isset($_FILES["tarifs"])) {
        $file = "tarifs";
    } else if (isset($_FILES["planning"])) {
        $file = "planning";
    }

    // check de si c'est une image ou pas
    if (substr(mime_content_type($_FILES[$file]["tmp_name"]), 0, 5) != "image") {
        $uploadOk = false;
        header('Location: ../information.php?erreur=1');
    }

    // check du poid du fichier
    if (($_FILES[$file]["size"] > 500000)) {
        $uploadOk = false;
        header('Location: ../information.php?erreur=2');
    }

    if ($uploadOk == true){
        // suppresion de l'ancien fichier
        $images = glob("../../res/fichiers/informations/" . $file . ".*");
        foreach ($images as $image) {
            unlink($image);
        }

        // ajout du nouveau fichier
        move_uploaded_file($_FILES[$file]["tmp_name"], "../../res/fichiers/informations/" . $file .'.'. explode(".", $_FILES[$file]["name"])[1]);
        header('Location: ../information.php?success=1');
    }
?>