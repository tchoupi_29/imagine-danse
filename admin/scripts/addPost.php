<?php
    session_start();
    include('../../connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }

    $uploadOk = true;

    $sth = $dbh->prepare('INSERT into post(titre, contenu, date_de_creation, heure_de_creation) values (?, ?, current_date, now())');
    $sth -> execute(array(htmlspecialchars("sfdkjgs"), htmlspecialchars("smfjdhkdfjg")));

    $sth = $dbh->prepare('SELECT MAX(id) FROM post');
    $sth -> execute();
    $idPostRes = $sth -> fetchAll();
    $idPost = $idPostRes[0]["MAX(id)"];
    print_r($idPostRes);

    $sth = $dbh->prepare('DELETE FROM post WHERE id = ?');
    $sth -> execute(array($idPost));

    $fichierPresent = false;
    foreach ($_FILES["fichiers"]["name"] as $name) {
        if (!empty($name)) {
            $fichierPresent = true;
        }
    }

    // on créer le dossier du post pour stoxker les fic joints
    mkdir('../../res/images/blog/' . ($idPost+1));

    // on set une variable de session pour retenir les champ si on retourne sur la page de creation
    $_SESSION["creationPost"] = array("contenu" => $_POST["contenu"], "titre" => $_POST["titre"]);

    if ($fichierPresent == true){
        $compteur = 0;
        // on ajout ces meme fic dans dossier
        for ($i=0; $i < sizeof($_FILES["fichiers"]["tmp_name"])-1 ; $i++) {
            // max file size : 8 Mo
            if ($_FILES["fichiers"]["size"][$i] > 8000000) {
                $uploadOk = false;
                header('Location: ../creerPost.php?erreur=1');
            }

            if ($compteur >= 20) {
                $uploadOk = false;
                header('Location: ../creerPost.php?erreur=2');
            }

            if ($uploadOk == true && !empty($_FILES["fichiers"]["tmp_name"][$i])) {
                $compteur = $compteur + 1;
                $nom = $i . ' ' .explode(".", $_FILES["fichiers"]["name"][$i])[0];
                move_uploaded_file($_FILES["fichiers"]["tmp_name"][$i], "../../res/images/blog/" . ($idPost+1) . "/" . $nom.'.'.explode(".", $_FILES["fichiers"]["name"][$i])[1]);
            }
        }
    }

    if ($uploadOk == true) {
        $sth = $dbh->prepare('INSERT into post(titre, contenu, date_de_creation, heure_de_creation) values (?, ?, current_date, now())');
        $sth -> execute(array(htmlspecialchars($_POST["titre"]), htmlspecialchars($_POST["contenu"])));
        unset($_SESSION["creationPost"]);
        header('Location: ../creerSuccess.php');
    }
?>
