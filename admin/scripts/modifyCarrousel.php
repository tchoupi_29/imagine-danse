<?php
    session_start();
    $uploadOk = true;

    $fichierPresent = false;
    if (isset($_FILES["fichiers"]["name"])) {
        foreach ($_FILES["fichiers"]["name"] as $name) {
            if (!empty($name)) {
                $fichierPresent = true;
            }
        }   
    }

    $maxFic = 0;
    $fics = array_diff(scandir("../../res/images/illustrations/carrousel/"), array(".", ".."));
    if (!empty($fics)) {
        foreach ($fics as $fic) {
            $nb = intval(substr($fic, 0, 2));
            if ($nb > $maxFic) {
                $maxFic = $nb;
            }
        }
    }

    if (isset($_POST["ficASuppr"])) {
        foreach ($_POST["ficASuppr"] as $fic) {
            unlink('../../res/images/illustrations/carrousel/' . $fic);
        }
    }

    if (sizeof(array_diff(scandir("../../res/images/illustrations/carrousel/"), array(".", ".."))) >= 20) {
        $uploadOk = false;
        header('Location: ../modifierCarrousel.php?erreur=3');
    }

    if ($fichierPresent == true){
        $compteur = 0;
        // on ajout ces meme fic dans dossier
        for ($i=0; $i < sizeof($_FILES["fichiers"]["tmp_name"])-1 ; $i++) {
            // max file size : 8 Mo
            if ($_FILES["fichiers"]["size"][$i] > 8000000) {
                $uploadOk = false;
                header('Location: ../modifierCarrousel.php?erreur=1');
            }

            if ($compteur >= 20) {
                $uploadOk = false;
                header('Location: ../modifierCarrousel.php?erreur=2');
            }

            // le fichier n'est pas une image
            
            if (!empty($_FILES["fichiers"]["tmp_name"][$i])) {
                if (substr(mime_content_type($_FILES["fichiers"]["tmp_name"][$i]), 0, 5) != "image"){
                    $uploadOk = false;
                    header('Location: ../modifierCarrousel.php?erreur=4');
                }
            }

            if ($uploadOk == true && !empty($_FILES["fichiers"]["tmp_name"][$i])) {
                $compteur = $compteur + 1;
                $nom = ($compteur + $maxFic) . ' ' .explode(".", $_FILES["fichiers"]["name"][$i])[0];
                move_uploaded_file($_FILES["fichiers"]["tmp_name"][$i], "../../res/images/illustrations/carrousel/" . $nom.'.'.explode(".", $_FILES["fichiers"]["name"][$i])[1]);
            }
        }
    }

    if ($uploadOk == true) {
        header('Location: ../modifierCarrouselSuccess.php');
    }
?>
