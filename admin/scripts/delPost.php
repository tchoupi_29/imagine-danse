<?php
    session_start();
    include('../../connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
    
    $sth = $dbh->prepare('DELETE from post where id=?');
    $sth -> execute(array($_GET["idPost"]));

    $imagesEtVideosDossier = array_diff(scandir("../../res/images/blog/".$_GET["idPost"]."/"), array(".", ".."));
    foreach ($imagesEtVideosDossier as $fichier) {
        unlink("../../res/images/blog/".$_GET["idPost"]."/".$fichier);
    }

    rmdir("../../res/images/blog/".$_GET["idPost"]."/");

    header('Location: ../blog.php');
?>