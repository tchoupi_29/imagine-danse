<?php
    require("../../scripts/inscription.php");

    session_start();
    include('../../connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage() . "<br/>";
        die();
    }
    

    if (insriptionStatus() == true) {
        $etat = 0;
    } else {
        $etat = 1;
    }

    $sth = $dbh->prepare('UPDATE inscription set ouverte = ? where id=1');
    $sth -> execute(array($etat));
    header('Location: ../inscription.php');
?>