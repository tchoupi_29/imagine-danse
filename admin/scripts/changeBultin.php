<?php
    session_start();
    $uploadOk = true;

    // check de si c'est un pdf ou pas
    if (substr(mime_content_type($_FILES["bultin"]["tmp_name"]), 12, 15) != "pdf") {
        $uploadOk = false;
        header('Location: ../inscription.php?erreur=1');
    }

    // check du poid du fichier
    if (($_FILES["bultin"]["size"] > 1000000)) {
        $uploadOk = false;
        header('Location: ../inscription.php?erreur=2');
    }

    if ($uploadOk == true){
        // suppresion de l'ancien bultin
        unlink("../../res/fichiers/inscription/bultin.pdf");

        // ajout du nouveau bultin
        move_uploaded_file($_FILES["bultin"]["tmp_name"], "../../res/fichiers/inscription/bultin.pdf");
        header('Location: ../inscription.php?success=1');
    }
?>