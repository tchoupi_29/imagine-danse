<header>
    <nav class="navbar navbar-light">
        <div class="container-fluid menu">
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Afficher le menu">
                <span class="line"></span> 
                <span class="line"></span> 
                <span class="line" style="margin-bottom: 0;"></span>
            </button>
            <div class="col-1">
                <a href="../index.php"><img class="logo" src="../res/images/icones/logoEntierBlanc.png" alt="logo imagine"></a>
            </div>
        </div>
    </nav>
    <div class="collapse" id="navbarToggleExternalContent">
        <div class="shadow-3 p-4">
            <div>
                <a href="administration.php">Accueil Administration</a>
            </div>
            <div>
                <a href="../index.php">Retour au site public</a>
            </div>
        </div>
    </div>
</header>