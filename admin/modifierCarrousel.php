<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Modification du Carrousel</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="modifierCarrousel.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <?php require('scripts/carrousel.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <form action="scripts/modifyCarrousel.php" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md">
                                <h3>Modification du Carrousel</h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <h4>Images du Carrousel <i onmouseover="afficheAide()" onmouseout="cacheAide()" class="bi-info-circle"></i></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="col-md-4">
                                    <p id="aide">
                                        Les images seront affichées dans le Carrousel sur la page d'accueil du site dans leur ordre d'ajout. La taille maximum d'un fichier est de 8 Mo, le nombre maximum d'images est de 20. Si aucune image n'est sélectionnée alors le Carrousel ne sera pas afficher.
                                    </p>
                                </div>
                            </div>
                            <?php
                                if (isset($_GET["erreur"])){
                                    if ($_GET["erreur"] == 1){
                                        echo '<div class="row">';
                                            echo '<div class="col-md-4">';
                                                echo '<p class="erreur">Un ou plusieurs fichier(s) sélectionné(s) est trop volumineux !</p>';
                                            echo '</div>';
                                        echo '</div>';
                                    } else if ($_GET["erreur"] == 2){
                                        echo '<div class="row">';
                                            echo '<div class="col-md-4">';
                                                echo '<p class="erreur">Il y a plus de 20 images !</p>';
                                            echo '</div>';
                                        echo '</div>';
                                    } else if ($_GET["erreur"] == 3){
                                        echo '<div class="row">';
                                            echo '<div class="col-md-4">';
                                                echo '<p class="erreur">Le nombre maximum d\'images a été atteint (20)</p>';
                                            echo '</div>';
                                        echo '</div>';
                                    } else if ($_GET["erreur"] == 4){
                                        echo '<div class="row">';
                                            echo '<div class="col-md-4">';
                                                echo '<p class="erreur">Un ou plusieurs fichier(s) sélectionné(s) n\'est pas une image !</p>';
                                            echo '</div>';
                                        echo '</div>';
                                    }
                                } 
                            ?>
                            <div class="row">
                                <div class="col-md-4" id="inputDiv">
                                    <!-- <input onchange="actuFile()" type="file" class="inputFichiers" name="fichiers[]" accept="image/*, video/*" multiple> -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 fichiers">
                                    <section id="tableauFic">
                                        <?php
                                            $images = getImagesCarrousel();
                                            if (!empty($images)) {
                                                echo '<p style="display: none;" id="aucunFichier">Aucun fichier sélectionné</p>';
                                                echo '<p style="display: none;" id="indInput">' . sizeof($images) . '</p>';
                                                $i = 2;
                                                foreach ($images as $key => $image) { 
                                                    echo '<div class="row align-items-center elemFichier" id="fichierDejaLa' . $i . '">';
                                                        echo '<div class="col-md-8">';
                                                            echo '<p>' . substr($image, 2) . '</p>';
                                                        echo '</div>';
                                                        echo '<div class="col-md-4 text-center">';
                                                            echo '<button class="btn btn-primary" onclick="supprimerFichierDejaLa(event, ' . $i . ', \'' . addslashes($image) . '\')">Supprimer</button>';
                                                        echo '</div>';
                                                    echo '</div>';
                                                    $i++;
                                                }
                                            } else {
                                                echo '<p id="aucunFichier">Aucun fichier sélectionné</p>';
                                                echo '<p style="display: none;" id="indInput">' . 0 . '</p>';
                                            }
                                        ?>
                                    </section>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md boutonValider">
                                <input type="submit" class="btn btn-primary" value="Modifier le Carrousel">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>
        <script>
            var input = document.getElementsByClassName("inputFichiers");

            var tableauFic = document.getElementById("tableauFic");
            var aucunFichier = document.getElementById("aucunFichier");
            var inputDiv = document.getElementById("inputDiv");

            var indInput = 0;
            var nbInput = Number(document.getElementById("indInput").innerText);
            var inputTab = [];

            function creatInput() {
                if (inputTab.length >= 1){
                    inputTab[indInput-1].style.display = "none";
                }

                inputCreate = document.createElement('input');
                inputCreate.setAttribute("onchange", "actuFile()");
                inputCreate.setAttribute("type", "file");
                inputCreate.setAttribute("class", "inputFichiers");
                inputCreate.setAttribute("name", "fichiers[]");
                inputCreate.setAttribute("accept", "image/*");
                inputCreate.setAttribute("id", "input".concat(indInput));

                inputTab.push(inputCreate);

                inputDiv.appendChild(inputTab[indInput]);

                indInput = indInput + 1;
            }

            for (let i = 0; i < nbInput; i++){
                creatInput();
            }

            creatInput();

            function actuTabFic() {
                // affiche ou pas l'indication si fichier ou pas
                pasDeFic = true;
                if (tableauFic.childNodes.length > 4){
                    pasDeFic = false;
                }
                if (pasDeFic == false) {
                    aucunFichier.style.display = "none";
                } else {
                    aucunFichier.style.display = "block";
                }
            }

            function actuFile() {
                
                // affiche tous les fichiers du input
                for (let fichier of inputTab[indInput-1].files) {
                    if (fichier.size > 8000000) {
                        inputTab[indInput-1].value = '';
                        inputDiv.removeChild(document.getElementById("input".concat(indInput-1)));
                        alert("Le fichier est trop volumineux ! (max : 8 Mo)");
                    } else {
                        // bouton qui déclenche une fonction qui enmève un elem du tableau de files
                        let boutonSupr = document.createElement("button");
                        boutonSupr.setAttribute("class", "btn btn-primary");
                        boutonSupr.setAttribute("onclick", "supprimerFichier(event, ".concat(indInput-1,')'));
                        boutonSupr.innerHTML = "Supprimer";

                        let row = document.createElement("div");
                        row.setAttribute("class", "row align-items-center elemFichier");
                        row.setAttribute("id", "fichier".concat(indInput-1));

                        let colText = document.createElement("div");
                        colText.setAttribute("class", "col-md-8");

                        let colBtn = document.createElement("div");
                        colBtn.setAttribute("class", "col-md-4 text-center");

                        let ligne = document.createElement("p");
                        ligne.innerText = fichier.name;

                        colText.appendChild(ligne);
                        colBtn.appendChild(boutonSupr);

                        row.appendChild(colText);
                        row.appendChild(colBtn);
                        tableauFic.appendChild(row);
                    }
                }
                actuTabFic();
                creatInput();
            }

            function supprimerFichier(event, ind) {
                event.preventDefault();
                inputTab[ind].value = '';
                tableauFic.removeChild(document.getElementById("fichier".concat(ind)));
                inputDiv.removeChild(document.getElementById("input".concat(ind)));
                actuTabFic();
            }

            function supprimerFichierDejaLa(event, ind, file) {
                event.preventDefault();

                let ligne = document.createElement("input");
                ligne.setAttribute("type", "hidden");
                ligne.setAttribute("name", "ficASuppr[]");
                ligne.setAttribute("value", file);
                inputDiv.appendChild(ligne);

                tableauFic.removeChild(document.getElementById("fichierDejaLa".concat(ind)));
                actuTabFic();
            }

            var aide = document.getElementById("aide");

            function afficheAide(){
                    aide.style.display = "block";
            }

            function cacheAide(){
                    aide.style.display = "none";
            }
        </script>
    </body>
</html>