<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Modification d'un Post</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="modifierPost.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <?php require('scripts/post.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <form action="scripts/modifyPost.php" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-md">
                                <h3>Modification d'un Post</h3>
                            </div>
                        </div>
                        <?php
                            $post = getPostByID($_GET['idPost']);
                            $titre = $post["titre"];
                            $contenu = $post["contenu"];
                            if (!isset($_GET["erreur"]) && isset($_SESSION["modificationPost"])) {
                                unset($_SESSION["modificationPost"]);
                            } else if (isset($_SESSION["modificationPost"])) {
                                $contenu = $_SESSION["modificationPost"]["contenu"];
                                $titre = $_SESSION["modificationPost"]["titre"];
                            }
                        ?>
                        <div class="row">
                            <div class="col-md">
                                <h4>Titre du Post</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <input value=<?php echo '"' . $titre . '"' ?> style="width: 100%;" type="text" id="titre" name="titre" placeholder="Spectacle de danse" required>
                            </div>
                            <div class="col-md">
                                <span style="color: red;">*</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <h4>Contenu du Post</h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <textarea style="width: 100%;" id="contenu" name="contenu" rows="4" cols="50" placeholder="Le spectacle aura lieu le..."><?php echo $contenu ?></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <h4>Pièces Jointes <i onmouseover="afficheAide()" onmouseout="cacheAide()" class="bi-info-circle"></i></h4>
                            </div>
                        </div>
                        <div class="row">
                            <div class="row">
                                <div class="col-md-4">
                                    <p id="aide">
                                        Les images et les vidéos seront affichées dans le Post dans leur ordre d'ajout. Les autres fichiers seront ajoutés sous forme de liens de téléchargment en dessous du contenu du post, dans la section "Pièces Jointes". La taille maximum d'un fichier est de 8 Mo, le nombre maximum de Pièces Jointes est de 20.
                                    </p>
                                </div>
                            </div>
                            <?php
                                if (isset($_GET["erreur"])){
                                    if ($_GET["erreur"] == 1){
                                        echo '<div class="row">';
                                            echo '<div class="col-md-4">';
                                                echo '<p class="erreur">Un ou plusieurs fichier(s) sélectionné(s) est trop volumineux !</p>';
                                            echo '</div>';
                                        echo '</div>';
                                    } else if ($_GET["erreur"] == 2){
                                        echo '<div class="row">';
                                            echo '<div class="col-md-4">';
                                                echo '<p class="erreur">Il y a plus de 20 fichiers !</p>';
                                            echo '</div>';
                                        echo '</div>';
                                    }
                                } 
                            ?>
                            <div class="row">
                                <div class="col-md-4" id="inputDiv">
                                    <!-- <input onchange="actuFile()" type="file" class="inputFichiers" name="fichiers[]" accept="image/*, video/*" multiple> -->
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 fichiers">
                                    <section id="tableauFic">
                                        <?php
                                            $files = getFiles($_GET['idPost'])["all"];
                                            natsort($files);
                                            if (!empty($files)) {
                                                echo '<p style="display: none;" id="aucunFichier">Aucun fichier sélectionné</p>';
                                                echo '<p style="display: none;" id="indInput">' . sizeof($files) . '</p>';
                                                $i = 0;
                                                foreach ($files as $file) { 
                                                    echo '<div class="row align-items-center elemFichier" id="fichierDejaLa' . $i . '">';
                                                        echo '<div class="col-md-8">';
                                                            echo '<p>' . substr($file, 2) . '</p>';
                                                        echo '</div>';
                                                        echo '<div class="col-md-4 text-center">';
                                                            echo '<button class="btn btn-primary" onclick="supprimerFichierDejaLa(event, ' . $i . ', \'' . addslashes($file) . '\')">Supprimer</button>';
                                                        echo '</div>';
                                                    echo '</div>';
                                                    $i++;
                                                }
                                            } else {
                                                echo '<p id="aucunFichier">Aucun fichier sélectionné</p>';
                                                echo '<p style="display: none;" id="indInput">' . 0 . '</p>';
                                            }
                                        ?>
                                    </section>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md boutonValider">
                                <input  type="hidden" name="idPost" value="<?php echo $_GET['idPost'] ?>">
                                <input type="submit" class="btn btn-primary" value="Modifier le Post">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>
        <script>
            var input = document.getElementsByClassName("inputFichiers");

            var tableauFic = document.getElementById("tableauFic");
            var aucunFichier = document.getElementById("aucunFichier");
            var inputDiv = document.getElementById("inputDiv");

            var indInput = 0;
            var nbInput = Number(document.getElementById("indInput").innerText);
            var inputTab = [];

            function creatInput() {
                if (inputTab.length >= 1){
                    inputTab[indInput-1].style.display = "none";
                }

                inputCreate = document.createElement('input');
                inputCreate.setAttribute("onchange", "actuFile()");
                inputCreate.setAttribute("type", "file");
                inputCreate.setAttribute("class", "inputFichiers");
                inputCreate.setAttribute("name", "fichiers[]");
                inputCreate.setAttribute("accept", "*");
                inputCreate.setAttribute("id", "input".concat(indInput));

                inputTab.push(inputCreate);

                inputDiv.appendChild(inputTab[indInput]);

                indInput = indInput + 1;
            }

            for (let i = 0; i < nbInput; i++){
                creatInput();
            }

            creatInput();

            function actuTabFic() {
                // affiche ou pas l'indication si fichier ou pas
                pasDeFic = true;
                if (tableauFic.childNodes.length > 4){
                    pasDeFic = false;
                }
                if (pasDeFic == false) {
                    aucunFichier.style.display = "none";
                } else {
                    aucunFichier.style.display = "block";
                }
            }

            function actuFile() {
                
                // affiche tous les fichiers du input
                for (let fichier of inputTab[indInput-1].files) {
                    if (fichier.size > 8000000) {
                        inputTab[indInput-1].value = '';
                        inputDiv.removeChild(document.getElementById("input".concat(indInput-1)));
                        alert("Le fichier est trop volumineux ! (max : 8 Mo)");
                    } else {
                        // bouton qui déclenche une fonction qui enmève un elem du tableau de files
                        let boutonSupr = document.createElement("button");
                        boutonSupr.setAttribute("class", "btn btn-primary");
                        boutonSupr.setAttribute("onclick", "supprimerFichier(event, ".concat(indInput-1,')'));
                        boutonSupr.innerHTML = "Supprimer";

                        let row = document.createElement("div");
                        row.setAttribute("class", "row align-items-center elemFichier");
                        row.setAttribute("id", "fichier".concat(indInput-1));

                        let colText = document.createElement("div");
                        colText.setAttribute("class", "col-md-8");

                        let colBtn = document.createElement("div");
                        colBtn.setAttribute("class", "col-md-4 text-center");

                        let ligne = document.createElement("p");
                        ligne.innerText = fichier.name;

                        colText.appendChild(ligne);
                        colBtn.appendChild(boutonSupr);

                        row.appendChild(colText);
                        row.appendChild(colBtn);
                        tableauFic.appendChild(row);
                    }
                }
                actuTabFic();
                creatInput();
            }

            function supprimerFichier(event, ind) {
                event.preventDefault();
                inputTab[ind].value = '';
                tableauFic.removeChild(document.getElementById("fichier".concat(ind)));
                inputDiv.removeChild(document.getElementById("input".concat(ind)));
                actuTabFic();
            }

            function supprimerFichierDejaLa(event, ind, file) {
                event.preventDefault();

                let ligne = document.createElement("input");
                ligne.setAttribute("type", "hidden");
                ligne.setAttribute("name", "ficASuppr[]");
                ligne.setAttribute("value", file);
                inputDiv.appendChild(ligne);

                tableauFic.removeChild(document.getElementById("fichierDejaLa".concat(ind)));
                actuTabFic();
            }

            var aide = document.getElementById("aide");

            function afficheAide(){
                    aide.style.display = "block";
            }

            function cacheAide(){
                    aide.style.display = "none";
            }
        </script>
    </body>
</html>