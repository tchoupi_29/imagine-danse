<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Détail d'un Post</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="detailPost.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md boutonRetour">
                            <a class="btn btn-primary" href="blog.php">Retour à la liste des Posts</a>
                        </div>
                    </div>
                    <?php
                        require('scripts/post.php');
                        $post = getPostByID($_GET["idPost"]);
                        echo '<div class="row art justify-content-center">';
                            echo '<div class="row titreArt">';
                            echo '<div class="col-md">';
                                    echo '<h3>'. $post["titre"] .'</h3>';
                                echo '</div>';
                                echo '<div class="col-md heurePubli">';
                                    echo '<p>' . $post["date_de_creation"] . ' '. $post["heure_de_creation"] . '</p>';
                                echo '</div>';
                            echo '</div>';
                            echo '<div class="row">';
                                echo '<div class="col-md">';
                                    echo '<div class="row">';
                                        echo '<p>' . $post["contenu"] . '</p>';
                                    echo '</div>';
                                    // variable
                                    $files = getFiles($post["id"]);
                                    $imagesEtVideos = $files["imagesEtVideos"];
                                    $piecesJointes = $files["pj"];
                                    if (sizeof($piecesJointes) > 0){
                                        echo '<div class="row">';
                                        echo '<details>';
                                        echo '<summary>Pièces Jointes</summary>';
                                        foreach ($piecesJointes as $pj) {
                                            echo '<div class="row">';
                                                echo '<a href="../res/images/blog/' . $post["id"] . '/' . $pj . '" download>' . $pj . '</a>';
                                            echo '</div>';
                                        }
                                        echo '</details>';
                                        echo '</div>';
                                    }
                                    if (sizeof($imagesEtVideos) > 0){
                                        foreach ($imagesEtVideos as $imageOuVideo) {
                                            if (substr(mime_content_type('../res/images/blog/' . $post["id"] . '/' . $imageOuVideo), 0, 5) == "image"){
                                                echo '<div class="row justify-content-center">';
                                                    echo '<div class="col-md imageArt">';
                                                        echo '<a href="../res/images/blog/' . $post["id"] . '/' . $imageOuVideo . '"><img src="../res/images/blog/' . $post["id"] . '/' . $imageOuVideo . '" alt="' . $post["titre"] . '"></a>';
                                                    echo '</div>';
                                                echo '</div>';
                                            } else {
                                                echo '<div class="row justify-content-center">';
                                                    echo '<div class="col-md imageArt">';
                                                        echo '<video width="320" height="240" controls>';
                                                            echo '<source src="../res/images/blog/' . $post["id"] . '/' . $imageOuVideo . '" type="video/mp4">';
                                                            echo 'Your browser does not support the video tag.';
                                                        echo '</video>';
                                                    echo '</div>';
                                                echo '</div>';
                                            }
                                        }
                                    }
                                echo '</div>';
                            echo '</div>';
                        echo '</div>';
                    ?>
                </div>
            </div>
        </main>
    </body>
</html>