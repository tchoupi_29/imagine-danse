<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Administration</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="administration.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md">
                            <h3>Administration</h3>
                        </div>
                    </div>
                    <a href="blog.php" class="row">
                        <div class="col-md">
                            <h4>Administration du Blog</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p>
                                    Créez, modifiez et suprimez des posts du blog.
                                </p>
                            </div>
                        </div>
                    </a>
                    <a href="inscription.php" class="row">
                        <div class="col-md">
                            <h4>Administration des Inscriptions</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p>
                                    Ouvrez ou fermez les inscriptions, modifiez le bulletin d'inscription.
                                </p>
                            </div>
                        </div>
                    </a>
                    <a href="information.php" class="row">
                        <div class="col-md">
                            <h4>Administration des Informations Pratiques</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p>
                                    Modifiez les différents tableaux des informations pratiques.
                                </p>
                            </div>
                        </div>
                    </a>
                    <a href="modifierCarrousel.php" class="row">
                        <div class="col-md">
                            <h4>Administration du Carrousel</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p>
                                    Modifiez les différentes images du Carrousel.
                                </p>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </main>
    </body>
</html>