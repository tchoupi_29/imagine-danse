<?php
    session_start();
    include('../connectParams.php');
    try {
        $dbh = new PDO("$driver:host=$server;dbname=$dbname", $user, $pass);
        
        $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $dbh -> setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    } catch (PDOException $e) {
        print "Erreur : " . $e->getMessage();
        die();
    }
    $fileContent = file_get_contents("../bdd/script.sql");
    $dbh->exec($fileContent);

    $imageDir = scandir("../res/images/");
    if (!in_array("blog", $imageDir)){
        mkdir("../res/images/blog");
    }

    //supression des images déjà là
    $dirImgProd = scandir('../res/images/blog');
    $dirImgProd = array_diff($dirImgProd, array('.', '..'));
    foreach ($dirImgProd as $dir) {
        $imgProd = scandir('../res/images/blog/'.$dir);
        $imgProd = array_diff($imgProd, array('.', '..'));
        foreach ($imgProd as $img) {
            unlink('../res/images/blog/' . $dir . '/' . $img);
        }
        rmdir('../res/images/blog/'.$dir);
    }

    header("Location: ./administration.php" );
?>