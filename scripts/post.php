<?php
    function getPost($page, $nb=20){
        global $dbh;
        $sth = $dbh->prepare('SELECT * from post order by id');
        $sth -> execute();
        $posts = $sth -> fetchAll();

        $dernierPostDeLaSerie = $page * $nb;
        
        if (sizeof($posts) - $dernierPostDeLaSerie < $nb){
            $nb = sizeof($posts) - $dernierPostDeLaSerie;
        }

        $newPosts = [];
        for ($i=sizeof($posts) - $dernierPostDeLaSerie - 1; $i > (sizeof($posts)-1) - $dernierPostDeLaSerie - $nb; $i--) { 
            $newPosts[] = $posts[$i];
        }
        return $newPosts;
    }

    function getFiles($idPost){
        $imagesEtVideosDossier = array_diff(scandir("res/images/blog/".$idPost."/"), array(".", ".."));

        $imagesEtVideos = [];
        $pj = [];
        if (sizeof($imagesEtVideosDossier) > 0){
            foreach ($imagesEtVideosDossier as $fichier) {
                if (substr(mime_content_type("res/images/blog/".$idPost."/".$fichier), 0, 5) == "image" || substr(mime_content_type("res/images/blog/".$idPost."/".$fichier), 0, 5) == "video"){
                    $imagesEtVideos[] = $fichier;
                } else {
                    $pj[] = $fichier;
                }
            }
        }
        return array('imagesEtVideos' => $imagesEtVideos, 'pj' => $pj);
    }

    function getMaxPage($nb=20){
        global $dbh;
        $sth = $dbh->prepare('SELECT * from post order by id');
        $sth -> execute();
        $posts = $sth -> fetchAll();
        
        return intdiv(sizeof($posts), $nb);
    }
?>