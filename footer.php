<footer class="container-fluid">
    <div class="row text-center">
        <div class="col-md">
            <div class="row">
                <div class="col-md">
                    <p>
                        Retrouvez nous sur les <a class="lienChache" href="admin/administration.php">r</a>éseaux sociaux
                    </p>
                </div>
                <div class="col-md-2">
                    <a target="_blank" href="https://www.facebook.com/imagine.danse">
                        <img src="res/images/icones/fb.png"  alt="logo facebook">
                    </a>
                </div>
                <div class="col-md-2">
                    <a target="_blank" href="https://www.instagram.com/imagine_danse/">
                        <img src="res/images/icones/insta.png"  alt="logo instagram">
                    </a>
                </div>
            </div>
        </div>
        <div class="col-md">
            <p class="m-0">
                Site Web réalisé par <a target="_blank" href="https://gitlab.com/tchoupi_29/imagine-danse">Tristan Chardès</a>
                <br>
                2022
            </p>
        </div>
        <div class="col-md">
            <a href="contact.php">
                <p>
                    Nous contacter
                </p>
            </a>
        </div>
    </div>
</footer>