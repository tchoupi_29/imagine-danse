<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Informations Pratiques</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="informations.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md">
                            <h3>Informations Pratiques</h3>
                        </div>
                    </div>
                    <?php
                        $horaires = glob("res/fichiers/informations/horaires.*");
                        $tarifs = glob("res/fichiers/informations/tarifs.*");
                        $planning = glob("res/fichiers/informations/planning.*");
                    ?>
                    <div class="row">
                        <div class="col-md">
                            <h4>Les Horaires</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a target="_blank" href="res/fichiers/informations/horaires.<?php echo pathinfo($horaires[0])["extension"] ?>"><img src="res/fichiers/informations/horaires.<?php echo pathinfo($horaires[0])["extension"] ?>" alt="les horaires"></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <h4>Les Tarifs</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a target="_blank" href="res/fichiers/informations/tarifs.<?php echo pathinfo($tarifs[0])["extension"] ?>"><img src="res/fichiers/informations/tarifs.<?php echo pathinfo($tarifs[0])["extension"] ?>" alt="les tarifs"></a>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <p>
                                    Règlement annuel à l'ordre de Imagine Danse<br>
                                    Nous acceptons le paiement en 3 fois, ainsi que les chèques vacances et les coupons sport.
                                </p>
                                <p>
                                    Possibilité de 3 chèques établis en début d'année, débités comme suit :
                                </p>
                                <ul>
                                    <li>
                                        1er chèque + cotisation : encaissement 10 Octobre
                                    </li>
                                    <li>
                                        2ème chèque : encaissement 10 Novembre
                                    </li>
                                    <li>
                                        3ème chèque : encaissement 10 Février
                                    </li>
                                </ul>
                                <p>
                                    Toute année commencée est due dans son intégralité, sauf sur présentation d'un certificat médical.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <h4>Le Planning de l'Année</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <a target="_blank" href="res/fichiers/informations/planning.<?php echo pathinfo($planning[0])["extension"] ?>"><img src="res/fichiers/informations/planning.<?php echo pathinfo($planning[0])["extension"] ?>" alt="les plannings"></a>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <h4>Renseignements Complémentaires</h4>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <ul>
                                    <li>
                                        Professeur de danse : <a href="decouvrir.php#professeure">Mélanie Gambie</a>
                                    </li>
                                    <li>
                                        Lieu des cours : <a href="contact.php">salle de Danse et des Arts - Boulevard de la République - Landivisiau</a>
                                    </li>
                                    <li>
                                        Tenue pour les cours (au choix) :
                                        <ul>
                                            <li>
                                                Un T-shirt + un caleçon ou un collant sans pied
                                            </li>
                                            <li>
                                                Un justaucorps + un collant sans pied
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php require('footer.php'); ?>
    </body>
</html>