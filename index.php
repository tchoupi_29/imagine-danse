<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Imagine Danse...</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="index.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row justify-content-center">
                        <div class="col">
                            <?php
                                $images = array_diff(scandir("res/images/illustrations/carrousel/"), array(".", ".."));
                                natsort($images);
                                $affichageCarous = "";
                                if (sizeof($images) == 0){
                                    $affichageCarous = "display : none;";
                                }
                            ?>
                            <div id="carouselExampleIndicators" style="<?php echo $affichageCarous ?>" class="carousel carousel-dark slide" data-bs-ride="carousel">
                                <div class="carousel-indicators">
                                    <?php
                                        $premiereImage = true;
                                        $compteur = 0;
                                        foreach ($images as $image) {
                                            if ($premiereImage == true){
                                                $premiereImage = false;
                                                echo '<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="'.$compteur.'" class="active" aria-current="true" aria-label="Slide ' . $compteur + 1 . '"></button>';
                                            } else {
                                                echo '<button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="'.$compteur.'" aria-label="Slide ' . $compteur + 1 . '"></button>';
                                            }
                                            $compteur = $compteur + 1;
                                        }
                                    ?>
                                </div>
                                <div class="carousel-inner">
                                    <?php
                                        $premiereImage = true;
                                        foreach ($images as $image) {
                                            if ($premiereImage == true){
                                                $premiereImage = false;
                                                echo '<div class="carousel-item active">';
                                            } else {
                                                echo '<div class="carousel-item">';
                                            }
                                            echo '<img src="res/images/illustrations/carrousel/' . $image . '" class="d-block" alt="' . $image . '">';
                                            echo '</div>';
                                        }
                                    ?>
                                </div>
                                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Précédent</span>
                                </button>
                                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Suivant</span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- affichages des post -->
                    <?php
                        require('scripts/post.php');
                        if (!isset($_GET["page"])){
                            $page = 0;
                        } else {
                            $page = $_GET["page"];
                        }
                        $posts = getPost($page, 20);
                        foreach ($posts as $post) {
                            echo '<div class="row art justify-content-center">';
                                echo '<div class="row titreArt">';
                                echo '<div class="col-md">';
                                        echo '<h3>'. $post["titre"] .'</h3>';
                                    echo '</div>';
                                    echo '<div class="col-md heurePubli">';
                                        echo '<p>' . $post["date_de_creation"] . ' '. $post["heure_de_creation"] . '</p>';
                                    echo '</div>';
                                echo '</div>';
                                echo '<div class="row">';
                                    echo '<div class="col-md">';
                                        echo '<div class="row">';
                                            echo '<p>' . $post["contenu"] . '</p>';
                                        echo '</div>';
                                        // variable
                                        $files = getFiles($post["id"]);
                                        $imagesEtVideos = $files["imagesEtVideos"];
                                        $piecesJointes = $files["pj"];
                                        if (sizeof($piecesJointes) > 0){
                                            echo '<div class="row">';
                                            echo '<details>';
                                            echo '<summary>Pièces Jointes</summary>';
                                            foreach ($piecesJointes as $pj) {
                                                echo '<div class="row">';
                                                    echo '<a href="res/images/blog/' . $post["id"] . '/' . $pj . '" download>' . $pj . '</a>';
                                                echo '</div>';
                                            }
                                            echo '</details>';
                                            echo '</div>';
                                        }
                                        if (sizeof($imagesEtVideos) > 0){
                                            foreach ($imagesEtVideos as $imageOuVideo) {
                                                if (substr(mime_content_type('res/images/blog/' . $post["id"] . '/' . $imageOuVideo), 0, 5) == "image"){
                                                    echo '<div class="row justify-content-center">';
                                                        echo '<div class="col-md imageArt">';
                                                            echo '<a target="_blank" href="res/images/blog/' . $post["id"] . '/' . $imageOuVideo . '"><img src="res/images/blog/' . $post["id"] . '/' . $imageOuVideo . '" alt="' . $post["titre"] . '"></a>';
                                                        echo '</div>';
                                                    echo '</div>';
                                                } else {
                                                    echo '<div class="row justify-content-center">';
                                                        echo '<div class="col-md imageArt">';
                                                            echo '<video width="320" height="240" controls>';
                                                                echo '<source src="res/images/blog/' . $post["id"] . '/' . $imageOuVideo . '" type="video/mp4">';
                                                                echo 'Your browser does not support the video tag.';
                                                            echo '</video>';
                                                        echo '</div>';
                                                    echo '</div>';
                                                }
                                            }
                                        }
                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                        }
                    ?>
                    <div class="row">
                        <div class="col-md text-center controlPage">
                            <?php
                                $maxPage = getMaxPage();
                                if ($page - 1 >= 0){
                                    echo '<a href="index.php?page=' . $page - 1 . '">←</a>';
                                }
                                if ($maxPage > 0) {
                                    echo $page+1 . '/' . $maxPage+1;
                                }
                                if ($page + 1 <= $maxPage){
                                    echo '<a href="index.php?page=' . $page + 1 . '">→</a>';
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php require('footer.php'); ?>
    </body>
</html>
