<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Contact</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="contact.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md">
                            <h3>Informations de contact</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <h4>Coordonnées</h4>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <ul>
                                    <li>
                                        <p>Mail : <a href="mailto:imagine.danse@gmail.com">imagine.danse@gmail.com</a></p>
                                    </li>
                                    <li>
                                        <p>Lieu des cours : Maison des Arts et de la Danse</p>
                                    </li>
                                    <ul>
                                        <li>
                                            <p>Adresse : <a target="_blank" href="https://goo.gl/maps/GUC5AoTBTbLXcbGe9">63 Boulevard de la République, 29400 Landivisiau</a></p>
                                        </li>
                                    </ul>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <h4>Réseaux Sociaux</h4>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <ul>
                                    <li>
                                        <p>Facebook : <a target="_blank" href="https://www.facebook.com/imagine.danse">imagine.danse</a></p>
                                    </li>
                                    <li>
                                        <p>Instagram : <a target="_blank" href="https://www.instagram.com/imagine_danse/">imagine_danse</a></p>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md text-center">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1869.220236233322!2d-4.072695225210123!3d48.504926465120015!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4811501301332415%3A0x6144b6b118d871d0!2sMaison%20Des%20Arts%20Et%20De%20La%20Danse!5e0!3m2!1sfr!2sfr!4v1671802654705!5m2!1sfr!2sfr" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php require('footer.php'); ?>
    </body>
</html>