<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Inscriptions</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="inscription.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md">
                            <h3>Inscriptions</h3>
                        </div>
                    </div>
                    <?php
                        require("scripts/inscription.php");
                        $status = insriptionStatus();
                        if ($status == false){
                            $dNone = ' style="display: none"';
                        } else {
                            $dNone = '';
                        }

                        if ($status == false) {
                            echo '<div class="row">';
                            echo '<div class="col-md-6">';
                            echo '<p>Les inscriptions sont fermées en ce moment.</p>';
                        }
                    ?>
                    <div class="row" <?php echo $dNone ?> >
                        <div class="col-md-6">
                            <p>
                                Pour l'inscription, nous vous demandons les documents suivants :
                            </p>
                            <ul>
                                <li>
                                    <p>
                                        Le bulletin d'inscription renseigné et signé
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Le certificat médical de moins de 3 mois obligatoire
                                    </p>
                                </li>
                                <li>
                                    <p>
                                        Le règlement de la cotisation annuelle (1 ou 3 chèques)
                                    </p>
                                </li>
                            </ul>
                            <p>
                                Merci de rendre votre dossier dûment complété à la rentrée. Aucune inscription ne sera validée si il manque un document.
                            </p>
                            <p>
                                Le document "informations et tarifs" ainsi que le règlement intérieur sont à conserver par les parents.
                            </p>
                            <p>
                                Merci de votre compréhension.
                            </p>
                        </div>
                    </div>
                    <div class="row justify-content-center" <?php echo $dNone ?>>
                        <div class="col-md text-center">
                            <a class="btn btn-primary" href="res/fichiers/inscription/bultin.pdf" download>Télécharger le<br>bulletin d'inscription</a>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php require('footer.php'); ?>
    </body>
</html>