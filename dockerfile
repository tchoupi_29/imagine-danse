FROM php:apache
RUN docker-php-ext-install pdo_mysql
COPY . /var/www/html
RUN mv res ras
COPY ./php.ini /usr/local/etc/php/
COPY ./000-default.conf /etc/apache2/sites-enabled/
COPY .htpasswd /etc/htpasswd/

RUN rm .htpasswd
RUN chown -R www-data:www-data /var/www/html/