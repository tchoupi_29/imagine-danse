<!DOCTYPE html>
<html lang="fr" xml:lang="fr" xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title>Découvrir Imagine Danse...</title>
        <?php require('head.php'); ?>
        <link rel="stylesheet" type="text/css" href="decouvrir.css" media="screen">
    </head>
    <body>
        <?php require('header.php'); ?>
        <main class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-md">
                            <h3>Découvrir Imagine Danse...</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <h4>Présentation de l'Association</h4>
                        </div>
                        <div class="row">
                            <div class="col-md intro">
                                <img src="res/images/icones/logoPetit.png" alt="logo d'imagine">
                                <p>
                                Imagine… est une association loi 1901 dont l'objet est de participer à l'épanouissement de l'être humain par le développement artistique : il s'agit de développer une action formatrice, culturelle, pour les enfants, les adolescents et les adultes à travers la danse contemporaine.<br>
                                L'origine de l'association remonte à 2003 : Martine Travel, professeure de danse contemporaine, entourée d'un noyau de parents investis, décide de créer une structure, pour continuer de véhiculer cet esprit de la danse qui transmet à chacun la communication par le corps et le sens du partage.<br>

                                La danse est un langage, les cours doivent permettre de développer la capacité créative de chaque danseur, tout en leur apportant les outils techniques qui leur permettront de s'exprimer.<br> 

                                L'association Imagine se veut être un lieu d'apprentissage, de partage, de savoir-être à travers la danse. Notre volonté est de rendre la danse  accessible à tous, des débutants aux confirmés, des jeunes enfants aux séniors.
                                </p>
                            </div>
                        </div>
                        <div class="row justify-content-center infographie">
                            <div class="col-md text-center">
                                <img src="res/images/illustrations/infographie/1 equipe.png" alt="1 equipe">
                            </div>
                            <div class="col-md text-center">
                                <img src="res/images/illustrations/infographie/9 membres.png" alt="9 membres">
                            </div>
                            <div class="col-md text-center">
                                <img src="res/images/illustrations/infographie/10 groupes.png" alt="10 groupes">
                            </div>
                            <div class="col-md text-center">
                                <img src="res/images/illustrations/infographie/20 ans.png" alt="20 ans">
                            </div>
                            <div class="col-md text-center">
                                <img src="res/images/illustrations/infographie/120 adhérents.png" alt="120 adhérents">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md" id="professeure">
                            <h4>La Professeure : Mélanie Gambie</h4>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <p>
                                    Du texte
                                </p>
                            </div>
                        </div>
                        <div class="row justify-content-center infographie">
                            <div class="col-md-3 text-center">
                                <img class="imgMelanie" src="res/images/illustrations/mélanie.jpg" alt="mélanie">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md">
                            <h4>Description des Cours</h4>
                        </div>
                        <div class="row">
                            <div class="col-md">
                                <p>
                                Chaque année, la progression pédagogique est rythmée par des temps forts :
                                </p>
                                <ul>
                                    <li>
                                        Trois fois par an, des ateliers parents-enfants sont proposés : il s'agit d'un moment où parents et enfants peuvent juste être là pour savourer, s'amuser ensemble, se rencontrer autrement
                                    </li>
                                    <li>
                                        En décembre, une séance ouverte permet de présenter le travail du premier trimestre et expliquer la démarche pédagogique aux parents
                                    </li>
                                    <li>
                                        La participation à des stages animés par des professionnels sont proposés régulièrement
                                    </li>
                                    <li>
                                        Lorsque c'est possible, l'association propose également d'aller voir des spectacles afin de d'enrichir les  la culture de la danse
                                    </li>
                                    <li>
                                        Imagine… prend régulièrement part à des partenariats avec le service culturel et le tissu associatif de Landivisiau 
                                    </li>
                                    <li>
                                        Le spectacle de fin d'année se tient traditionnellement fin juin 
                                    </li>
                                </ul>
                                <h5>Éveil</h5>
                                <p>
                                    Ce cours permet aux enfants d'acquérir de façon ludique les bases de la danse : connaître son corps, bien utiliser son espace de danse et respecter celui des autres, suivre la musique, le rythme, varier les qualités de mouvement, développer sa créativité, être autonome dans sa danse et s'intégrer dans un groupe.
                                </p>
                                <h5>Initiation</h5>
                                <p>
                                    Pendant le cours, l'enfant est amené à développer sa perception, son observation et son écoute. Ce cours permet aux enfants d'acquérir les bases qui permettront d'accéder plus tard à la technique. Ces thèmes sont abordés dans le cours, de manière ludique et sur des musiques diverses. 
                                </p>
                                <h5>À artir de 8 ans jusqu'à l'âge adulte</h5>
                                <p>
                                    Ce cours propose un travail portant sur le placement du corps et sur les éléments techniques propres à la danse contemporaine. Explorer, se surprendre soi-même, se faire plaisir en dansant… Ce cours conjugue apprentissage technique, composition et improvisation dans une conscientisation du corps et du mouvement dansé, à partir des notions fondamentales d'espace, de temps, de poids et de dynamique.
                                </p>
                                <h5>Les séances se déroulent de la manière suivante </h5>
                                <ul>
                                    <li>
                                        Echauffement et apprentissage de différentes techniques,
                                    </li>
                                    <li>
                                        Travail de déplacement et de coordination dans l'espace,
                                    </li>
                                    <li>
                                        Enchaînement chorégraphique
                                    </li>
                                    <li>
                                        Travail d'atelier
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <?php require('footer.php'); ?>
    </body>
</html>